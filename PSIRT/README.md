# README #

### Requirements -
You are required to have installed python, pip and pipenv.

For information about installing these, see the following links - 

- https://www.python.org/downloads/
- https://pypi.org/project/pip/
- https://pypi.org/project/pipenv/

### Execution -
Once all of the requirements have been met, open a command prompt and navigate to the current directory.

Run <code>pipenv install</code> to download the python modules needed to run.

Once complete, run <code>pipenv run update_psirt.py</code>

When prompted, enter your Cisco PSIRT API id and secret, obtainable with a free Cisco account from https://apiconsole.cisco.com/apps/mykeys

When prompted, enter the path to the directory Nipper is installed to. Do not include the executable.
Eg. <code>C:\Program Files\NipperStudio</code>

Nipper will now be updated with an up-to-date PSIRT resource file.