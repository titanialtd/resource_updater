#############################################################################################
# LICENCE                                                                                   #
# Copyright Titania Ltd 2021. You are free to copy and distribute this script. If you make  #
# any modifications to the script, these must be sent back to Titania for integration and   #
# potentially made available within future publicly available releases.                     #
#############################################################################################

import requests
import os
import subprocess
import urllib3


def update_psirt():
    """
    Retrieve an up to date Cisco PSIRT resource, and install it into Nipper - removing the old resource.
    """
    token = get_access_token()
    print("Downloading PSIRT resource file...")
    resource = get_resource(token)

    # Write resource to a temporary file. Note: Nipper does not like using python's tempfile
    temp_path = "CiscoPSIRT.json"
    try:
        with open(temp_path, mode="wt", encoding="utf-8") as temp_file:
            temp_file.write(resource)
        install_resource(temp_path)
    finally:
        os.remove(temp_path)


def get_id_secret_from_user():
    """
    Retrieve the user's PSIRT API id and secret
    :return: id, secret
    """
    api_id = input("Enter your Cisco PSIRT API id:\n")
    api_secret = input("Enter your Cisco PSIRT secret:\n")
    return api_id, api_secret


def get_access_token():
    """
    HTTP post containing API id and secret to obtain a limited time access token
    :return: access token
    """
    api_id, api_secret = get_id_secret_from_user()
    url = "https://cloudsso.cisco.com/as/token.oauth2"
    params = {"client_id": api_id, "client_secret": api_secret}
    data = {"grant_type": "client_credentials"}
    session = requests.Session()
    # Disable SSL verification and warnings
    session.verify = False
    urllib3.disable_warnings()
    r = session.post(url, data=data, params=params)
    if r.status_code != 200:
        print(f"Error while retrieving access token: {r}")
        exit(1)
    return r.json()['access_token']


def get_resource(token):
    """
    HTTP get containing time limited access token to retrieve all advisories
    :param token: access token
    :return: Full PSIRT resource as json
    """
    url = "https://api.cisco.com/security/advisories/all"
    headers = {"Accept": "application/json", "Authorization": ("Bearer " + token)}
    session = requests.Session()
    # Disable SSL verification and warnings
    session.verify = False
    urllib3.disable_warnings()
    r = session.get(url, headers=headers)
    if r.status_code != 200:
        print(f"Error while retrieving resource: {r}")
        exit(1)
    return r.text


def install_resource(resource):
    """
    Install the new PSIRT resource to Nipper, removing the old one first
    :param resource: Path to new resource file
    """
    my_environ = os.environ.copy()
    nipper_path = input("Enter the path to nipper.exe without quotation marks. Eg.\nC:\\Program Files\\NipperStudio\n")
    my_environ['PATH'] = nipper_path + ';' + my_environ['PATH']
    subprocess.call('nipper --resource-remove --rname="Cisco PSIRT Advisories"', shell=True, env=my_environ)
    subprocess.call('nipper --resource-remove --rname="CiscoPSIRT.json"', shell=True, env=my_environ)
    subprocess.call(f'nipper --resource-add --rname="Cisco PSIRT Advisories" --rcategory=PSIRT --rfilename="{resource}"',
                    shell=True, env=my_environ)


if __name__ == '__main__':
    """
    Program entry point
    """
    print("This script will update Nipper's PSIRT resource file. It will make two https requests to cisco.com -"
          " ignoring any SSL verification warnings.")
    update_psirt()